# x11vnc
#### This is my configuration to be used in ArchLinux but you are free to use it in other Linux distributions such as Debian LinuxMint among others.
------

- Do all setup not run as root.

- If you use [firewalld](https://wiki.archlinux.org/title/Firewalld) we will open port 5900.
Obs: this is the configuration for my firewall.

- Install [x11vnc](https://wiki.archlinux.org/title/X11vnc) `sudo pacman -S x11vnc`.

- Setting the password in command `x11vnc -storepasswd`.

- Enable and run the x11vnc `sudo systemctl enable --now x11vncd.service` and checking if everything is ok `sudo systemctl status x11vncd.service`.

- more about [x11vnc](https://wiki.archlinux.org/title/X11vnc) and [firewalld](https://wiki.archlinux.org/title/Firewalld)
