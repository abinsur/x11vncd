#!/usr/bin/env bash
####################################
### abinsur[at]cryptolab[dot]net ###
####################################

#---variables
XUSER="$(< /etc/hostname)"
DF="\033[0;39m"   #default
R="\033[1;31m"    #red
G="\033[1;32m"    #green
Y="\033[1;33m"    #yellow
B="\033[1;34m"    #blue
C="\033[0;36m"    #cyan

#---not root
if [[ $EUID -eq 0 ]];then
	echo -e $R"Not run as root"$DF
	exit 1
fi

#---install x11vnc
sudo pacman -S --noconfirm --needed x11vnc
sleep 1
x11vnc -storepasswd

#---update firewall
if [[ -x "$(command -v /usr/bin/firewalld)" ]];then
	sudo firewall-cmd --permanent --zone=aux --add-port=5900/tcp
	sudo firewall-cmd --reload
	sudo echo -e $G"Firewall has been updated"$DF
fi

#---configure x11vnc
sudo install -v -m 0644 -o root -g root x11vnc.service -t /etc/systemd/system/
#---command sed -> search and replace
# -i - insert
#  s - search
#  g - global
sudo sed -i "s/XUSER/${XUSER}/g" /etc/systemd/system/x11vnc.service
sleep 1
sudo systemctl enable --now x11vnc.service

#---figlet -f slant x11vncd
echo -e $Y'
        ______                     __
   _  _<  <  /   ______  _________/ /
  | |/_/ // / | / / __ \/ ___/ __  /
 _>  </ // /| |/ / / / / /__/ /_/ /
/_/|_/_//_/ |___/_/ /_/\___/\__,_/
									<(")
'$DF

unset XUSER DF R G Y B C

exit 0
